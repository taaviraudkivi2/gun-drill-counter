# Gun Drill - Counter

A simple application designed for personal dry-fire gun drills, specifically tailored for members of the Estonian Defence League. This application helps improve weapon mag chaning speed with counters to track progress and performance.


### Need a Friend's Assistance for Progress Tracking
- Start a timer to measure the time taken for a magazine change or any other drill.
- Earn points and level up by completing drills within the set time limit. Faster completions result in more points.
- Lose a level if you fail to complete the drill within the required time.