"use client";
import React, { useState } from "react";
import CompleteButton from "../components/CompleteButton";
import PointsCounter from "../components/PointsCounter";
import Levels from "../components/Levels";
import TimerButton from "../components/TimerButton";

export default function Home() {
  const [currentLevel, setCurrentLevel] = useState(1);
  const [points, setPoints] = useState(0);
  const [timerActive, setTimerActive] = useState(false);
  const [timerKey, setTimerKey] = useState(0);
  const [failuresRemaining, setFailuresRemaining] = useState(3);

  const levels = [
    { level: 1, timerDuration: 30, pointsOnSuccess: 1 },
    { level: 2, timerDuration: 20, pointsOnSuccess: 2 },
    { level: 3, timerDuration: 15, pointsOnSuccess: 3 },
    { level: 4, timerDuration: 10, pointsOnSuccess: 5 },
    { level: 5, timerDuration: 5, pointsOnSuccess: 10 },
  ];

  const handleSuccess = () => {
    const currentLevelData = levels[currentLevel - 1];
    setPoints(points + currentLevelData.pointsOnSuccess);

    if (currentLevel < levels.length) {
      setCurrentLevel(currentLevel + 1);
      setTimerKey(timerKey + 1);
    } else {
      alert("Hea töö, alusta algusest!");

      setCurrentLevel(1);
      setTimerKey(timerKey + 1);
    }

    setTimerActive(false);
    setFailuresRemaining(3);
  };

  const handleFail = () => {
    if (currentLevel > 1) {
      if (failuresRemaining > 0) {
        setFailuresRemaining(failuresRemaining - 1);
        setTimerActive(false);
        setTimerKey(timerKey - 1);

        if (failuresRemaining === 1) {
          setFailuresRemaining(3);
          setCurrentLevel(currentLevel - 1);
        }
      }
    } else {
      setTimerActive(false);
      setTimerKey(timerKey - 1);
    }
  };

  const handleTimerClick = () => {
    setTimerActive(true);
  };

  const handleTimerEnd = () => {
    if (currentLevel < levels.length) {
      setCurrentLevel(currentLevel + 1);
      setTimerKey(timerKey + 1);
    }
    setTimerActive(false);
  };

  return (
    <main className="flex flex-col items-center justify-between min-h-screen p-2">
      <Levels level={currentLevel} />
      <PointsCounter points={points} />
      <div className="flex flex-col justify-end items-center w-full space-y-2 p-2">
        <TimerButton
          key={timerKey}
          onTimerEnd={handleTimerEnd}
          onClick={handleTimerClick}
          label="start timer"
          duration={levels[currentLevel - 1].timerDuration}
          active={timerActive}
        />
        <CompleteButton
          onClick={handleSuccess}
          label="success"
          color="bg-light-green"
          disabled={!timerActive}
          points={levels[currentLevel - 1].pointsOnSuccess}
        />
        <CompleteButton
          onClick={handleFail}
          label={`Fail (${failuresRemaining} left)`}
          color="bg-light-red"
          disabled={
            !timerActive || (currentLevel === 1 && failuresRemaining === 0)
          }
        />
      </div>
    </main>
  );
}
