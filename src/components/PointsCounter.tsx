interface PointsCounterProps {
  points: number;
}

const PointsCounter: React.FC<PointsCounterProps> = ({ points }) => {
  return (
    <div className="pt-10">
      <p className="text-5xl">{points}</p>
    </div>
  );
};

export default PointsCounter;
