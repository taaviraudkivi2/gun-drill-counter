interface LevelProps {
  level: number;
}

const Levels: React.FC<LevelProps> = ({ level }) => {
  return (
    <div className="pt-6 text-center">
      <h1 className="text-2xl ">SALVEDRILLID</h1>
      <h2 className="text-2xl pt-10">level {level}</h2>
    </div>
  );
};

export default Levels;
