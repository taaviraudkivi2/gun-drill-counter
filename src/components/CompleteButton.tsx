import React from "react";

interface CompleteButtonProps {
  onClick: () => void;
  label: string;
  color?: string;
  disabled?: boolean;
  points?: number;
}

const CompleteButton: React.FC<CompleteButtonProps> = ({
  onClick,
  label,
  color = "bg-white",
  disabled = false,
  points,
}) => {
  return (
    <button
      className={`w-full ${color} text-black text-lg outline outline-2 font-bold py-10 px-4 rounded ${
        disabled ? "opacity-50 cursor-not-allowed" : "hover:bg-gray-200"
      }`}
      onClick={onClick}
      disabled={disabled}
    >
      {label}
      {points !== undefined && ` (+${points} points)`}
    </button>
  );
};

export default CompleteButton;
