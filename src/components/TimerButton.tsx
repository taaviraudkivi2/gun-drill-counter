import React, { useState, useEffect, useRef } from "react";

interface TimerButtonProps {
  onClick: () => void;
  label: string;
  duration: number;
  onTimerEnd?: () => void;
  active: boolean;
}

const TimerButton: React.FC<TimerButtonProps> = ({
  onClick,
  label,
  duration,
  onTimerEnd,
  active,
}) => {
  const [timer, setTimer] = useState(duration);
  const [isActive, setIsActive] = useState(false);
  const intervalRef = useRef<NodeJS.Timeout>();

  useEffect(() => {
    setIsActive(active);
  }, [active]);

  useEffect(() => {
    if (isActive) {
      intervalRef.current = setInterval(() => {
        setTimer((prevTimer) => {
          if (prevTimer > 0) {
            return prevTimer - 1;
          } else {
            clearInterval(intervalRef.current!);
            setIsActive(false);
            if (onTimerEnd) {
              onTimerEnd();
            }
            return 0;
          }
        });
      }, 1000);
    }

    return () => {
      clearInterval(intervalRef.current!);
    };
  }, [isActive, onTimerEnd]);

  const handleButtonClick = () => {
    setIsActive(true);
    onClick();
  };

  const formatTime = (time: number): string => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  };

  return (
    <button
      className={`w-full font-bold py-10 px-4 rounded text-lg ${
        isActive ? "bg-white outline outline-2" : "bg-black text-white"
      }`}
      onClick={handleButtonClick}
    >
      {isActive ? formatTime(timer) : label}
    </button>
  );
};

export default TimerButton;
